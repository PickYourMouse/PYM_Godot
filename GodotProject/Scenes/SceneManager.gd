extends Node

# This script manage scenes, need to run as @@ Singleton script @@

class SceneInfo:
	var myName					:String				= ""
	var myResourcePath			:String				= ""
	var myInstance				:Node				= null

# scenes
var myCurrentScene		:SceneInfo 		= null
var myScenes	 		:Dictionary		= {}

# interfaces 
func get_current_scene() -> SceneInfo:
	if myCurrentScene == null:
		printerr("No scene is set as current")
	return myCurrentScene

func pre_load_scene(aName:String, aResourcePath:String):
	if _priv_exist_scene(aName):
		return
	_priv_pre_load_scene(aName, aResourcePath)

func load_scene(aName:String):
	if not _priv_exist_scene(aName):
		printerr("Scene %s not exist" % aName)
		return
	if _priv_scene_loaded(aName):
		return
	_priv_load_scene(aName)

func get_scene(aName:String) -> Node:
	if not _priv_exist_scene(aName):
		printerr("Scene %s not exist" % aName)
		return null
	if not _priv_scene_loaded(aName):
		_priv_load_scene(aName)
	return _priv_get_scene(aName).myInstance

func goto_scene(aName:String, aFreeCurrentScene:bool=false):
	if myCurrentScene.myName == aName:
		return
	if not _priv_scene_loaded(aName):
		_priv_load_scene(aName)
	# Deleting the current scene at this point is
	# a bad idea, because it may still be executing code.
	# This will result in a crash or unexpected behavior.
	
	# The solution is to defer the load to a later time, when
	# we can be sure that no code from the current scene is running:
	call_deferred("_priv_deffered_goto_scene", aName, aFreeCurrentScene)

# engine
func _ready():
	_priv_initialize_start_scene()
	pre_load_scene("TitleScene", "res://Scenes/TitleScene/TitleScene.tscn")
	pre_load_scene("MainMenuScene", "res://Scenes/MainMenuScene/MainMenuScene.tscn")
	pre_load_scene("PYMDemoScene", "res://Demos/PYMDemoScene/PYMDemoScene.tscn")

# dirty works
func _priv_initialize_start_scene():
	myCurrentScene = SceneInfo.new()
	myCurrentScene.myName = "StartScene"
	myCurrentScene.myResourcePath = "res://Scenes/StartScene/StartScene.tscn"
	var root = get_tree().get_root()
	myCurrentScene.myInstance = root.get_child(root.get_child_count() - 1)
	_priv_add_scene(myCurrentScene)

func _priv_exist_scene(aName:String) -> bool:
	return myScenes.has(aName)

func _priv_scene_loaded(aName:String) -> bool:
	return myScenes.has(aName) and myScenes[aName].myInstance != null

func _priv_pre_load_scene(aName:String, aResourcePath:String):
	var scene = SceneInfo.new()
	scene.myName = aName
	scene.myResourcePath = aResourcePath
	myScenes[scene.myName] = scene
	print("Pre-load scene %s from %s" % [aName, aResourcePath])

func _priv_load_scene(aName:String):
	var scene = myScenes[aName]
	var packedScene = load(scene.myResourcePath)
	scene.myInstance = packedScene.instance()
	print("Load scene %s" % aName)

func _priv_unload_scene(aName:String):
	myScenes[aName].myInstance = null

func _priv_add_scene(aInfo:SceneInfo):
	myScenes[aInfo.myName] = aInfo
	print("Add scene %s" % aInfo.myName)

func _priv_remove_scene(aName:String):
	myScenes.erase(aName)
	print("Remove scene %s" % aName)

func _priv_get_scene(aName:String) -> SceneInfo:
	return myScenes[aName]

func _priv_deffered_goto_scene(aName:String, aFreeCurrentScene:bool):
	if aFreeCurrentScene:
		myScenes.erase(myCurrentScene.myName)
		myCurrentScene.myInstance.free()
		print("Free SceneInfo %s" % myCurrentScene.myName)
	
	print("Goto scene %s" % aName)
	get_tree().get_root().remove_child(myCurrentScene.myInstance)
	myCurrentScene = myScenes[aName]
	get_tree().get_root().add_child(myCurrentScene.myInstance)
	get_tree().set_current_scene(myCurrentScene.myInstance)
