extends Node2D

# engine
func _ready():
	var ui = UIManager.get_ui("MainMenu")
	add_child(ui)
	get_tree().get_root().emit_signal("size_changed")
