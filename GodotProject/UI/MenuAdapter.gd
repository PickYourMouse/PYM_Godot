# This is deprecated, please use MenuBase instead
class_name MenuAdapter

var myMenu			:Control			= null

# interfaces
func register(aMenu:Control):
	if myMenu == null:
		aMenu.get_tree().get_root().connect('size_changed', self, "resize_menu")
	myMenu = aMenu

func resize_menu():
	var size = myMenu.get_viewport_rect().size
	myMenu.set_size(size)
