extends Node

class UIInfo:
	var myName					:String				= ""
	var myResourcePath			:String				= ""
	var myInstance				:Node				= null

# scenes
var myComponents	 		:Dictionary			= {}

# interfaces 
func pre_load_ui(aName:String, aResourcePath:String):
	if _priv_exist_ui(aName):
		return
	_priv_pre_load_ui(aName, aResourcePath)

func load_ui(aName:String):
	if not _priv_exist_ui(aName):
		printerr("Scene %s not exist" % aName)
		return
	if _priv_ui_loaded(aName):
		return
	_priv_load_ui(aName)

func get_ui(aName:String) -> Node:
	if not _priv_exist_ui(aName):
		printerr("Scene %s not exist" % aName)
		return null
	if not _priv_ui_loaded(aName):
		_priv_load_ui(aName)
	return _priv_get_ui(aName).myInstance

# engine
func _ready():
	pre_load_ui("MainMenu", "res://UI/MainMenu/MainMenu.tscn")
	if Configuration.myEnableDemoScene:
		pre_load_ui("MainMenu.ButtonStartDemo", "res://UI/MainMenu/ButtonStartDemo/ButtonStartDemo.tscn")

# dirty works
func _priv_exist_ui(aName:String) -> bool:
	return myComponents.has(aName)

func _priv_ui_loaded(aName:String) -> bool:
	return myComponents.has(aName) and myComponents[aName].myInstance != null

func _priv_pre_load_ui(aName:String, aResourcePath:String):
	var ui = UIInfo.new()
	ui.myName = aName
	ui.myResourcePath = aResourcePath
	myComponents[ui.myName] = ui
	print("Pre-load ui %s from %s" % [aName, aResourcePath])

func _priv_load_ui(aName:String):
	var ui = myComponents[aName]
	var packedScene = load(ui.myResourcePath)
	ui.myInstance = packedScene.instance()
	print("Load ui %s" % aName)

func _priv_unload_ui(aName:String):
	myComponents[aName].myInstance = null

func _priv_add_ui(aInfo:UIInfo):
	myComponents[aInfo.myName] = aInfo
	print("Add ui %s" % aInfo.myName)

func _priv_remove_ui(aName:String):
	myComponents.erase(aName)
	print("Remove ui %s" % aName)

func _priv_get_ui(aName:String) -> UIInfo:
	return myComponents[aName]