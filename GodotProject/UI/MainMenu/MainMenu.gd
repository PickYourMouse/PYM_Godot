extends MenuBase

func _ready():
	$OutBoarder/RegionStartGame/ButtonStartGame.connect("button_down", GameProcessController, "_on_start_game_button_down")
	$OutBoarder/RegionQuitGame/ButtonQuitGame.connect("button_down", GameProcessController, "_on_quit_game_button_down")

	if Configuration.myEnableDemoScene:
		var button = UIManager.get_ui("MainMenu.ButtonStartDemo")
		$OutBoarder.add_child(button)
		$OutBoarder.move_child(button, 0)
		$OutBoarder/RegionStartDemo/ButtonStartDemo.connect("button_down", GameProcessController, "_on_start_demo_button_down")

func resize():
	$OutBoarder.set_size(get_viewport_rect().size)
