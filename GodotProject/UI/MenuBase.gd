# A base class for Menu, we support resize feature for now
extends Control

class_name MenuBase

# engine
func _ready():
	get_tree().get_root().connect('size_changed', self, "_priv_on_resize")

# interface
func resize():
	pass

# events
func _priv_on_resize():
	resize()

# dirty works
