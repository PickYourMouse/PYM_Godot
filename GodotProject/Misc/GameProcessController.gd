extends Node

# This script controls Game Process, need to run as @@ the last Singleton script @@

func _ready():
	if not Configuration.myDebugOneScene:
		SceneManager.goto_scene("TitleScene")

func _on_start_game_button_down():
	print("start game")

func _on_quit_game_button_down():
	print("quit game")

func _on_start_demo_button_down():
	print("start demo")
	SceneManager.goto_scene("PYMDemoScene")
