extends MoveController2DBase

# required components
onready var myControlledNode 	:KinematicBody2D	= get_parent()

var myCurrentRawDirection		:Vector2 			= Vector2(0,0)
var myCurrentDirection			:Vector2 			= Vector2(0,0)

# engine
func _physics_process(aDeltaTime:float):
	# move
	var velocity = myCurrentDirection*mySpeed
	myControlledNode.move_and_slide(velocity)

func _input(aInputEvent:InputEvent):
	# left
	if aInputEvent.is_action_pressed('p1_left'):
		myCurrentRawDirection += Vector2.LEFT
	elif aInputEvent.is_action_released('p1_left'):
		myCurrentRawDirection -= Vector2.LEFT
	# right
	if aInputEvent.is_action_pressed('p1_right'):
		myCurrentRawDirection += Vector2.RIGHT
	elif aInputEvent.is_action_released('p1_right'):
		myCurrentRawDirection -= Vector2.RIGHT
	# up
	if aInputEvent.is_action_pressed('p1_up'):
		myCurrentRawDirection += Vector2.UP
	elif aInputEvent.is_action_released('p1_up'):
		myCurrentRawDirection -= Vector2.UP
	# down
	if aInputEvent.is_action_pressed('p1_down'):
		myCurrentRawDirection += Vector2.DOWN
	elif aInputEvent.is_action_released('p1_down'):
		myCurrentRawDirection -= Vector2.DOWN
	# dir
	myCurrentDirection = myCurrentRawDirection.normalized()
	myControlledNode.look_at(myControlledNode.get_position() + myCurrentDirection)

