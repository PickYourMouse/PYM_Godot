extends MoveController2DBase

class_name PathFollowMoveController2D

# required components
onready var myControlledNode 	:KinematicBody2D	= get_parent()

var myReachDistance		:float			= 1 	# distance used to determine whether reach target
var myPath				:Array			= [] 	# world positions list from start to target

# interfaces
func update_path(aPathInWorld:Array):
	myPath = aPathInWorld.duplicate()
	myControlledNode.set_position(myPath[0])
	myPath.remove(0)

# engine
func _ready():
	pass

func _physics_process(aDeltaTime:float):
	_priv_update_movement(aDeltaTime)

# dirty work
func _priv_update_movement(aDeltaTime:float):
	if myPath.size() != 0:
		var target = myPath[0]
		var curPos = myControlledNode.get_position()
		
		if curPos.distance_to(target) < myReachDistance:
			myPath.remove(0)
			if myPath.size() == 0:
				return
			target = myPath[0]
		
		var dir = (target - curPos).normalized()
		var velocity = dir * mySpeed
		var movement = velocity * aDeltaTime
		
		myControlledNode.look_at(target)
		# myControlledNode.set_position(curPos + movement)
		var colliders = myControlledNode.move_and_collide(movement)
		if colliders:
			print("path colliders : ", colliders)

