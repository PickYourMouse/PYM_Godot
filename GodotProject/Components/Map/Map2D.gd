extends TileMap

class_name Map2D

var myPath						:Array		= [] 			# myPath in loc map
var myAstar						:AStar		= AStar.new()
var myMapRegion					:Rect2		= Rect2()
var myWalkableIds				:Array		= []

# # provided by engine
# func world_to_map(aPositionInWorld:Vector2) -> Vector2:
#	# world position to map position
# 	return .world_to_map(aPositionInWorld)

# # provided by engine
# func map_to_world(aPositionInMap:Vector2, aIgnoreHalfOffset:bool=false) -> Vector2:
#	# position in map to wolrd position (cell corner)
#	# aIgnoreHalfOfs: ignore the tilemap's half offset, !!! not taking effect !!!
#	return .map_to_world(aPositionInMap, aIgnoreHalfOffset)

func map_to_world_center(aPositionInMap:Vector2) -> Vector2:
	# position in map to world position (tilemap cell center position)
	return map_to_world(aPositionInMap) + 0.5*get_cell_size()

func map_to_world_configured(aPositionInMap:Vector2, aReturnCellCenterPosition:bool=false) -> Vector2: # position in map to world (tilemap cell center position)
	if aReturnCellCenterPosition:
		return map_to_world_center(aPositionInMap)
	else:
		return map_to_world(aPositionInMap)

func worlds_to_maps(aPositionsInWorld:Array) -> Array:
	# world positions to map positions
	var positionsInMap = []
	for positionInWorld in aPositionsInWorld:
		positionsInMap.append(world_to_map(positionInWorld))
	return positionsInMap

func maps_to_worlds(aPositionsInMap:Array, aReturnCellCenterPosition:bool=false) -> Array:
	# positions in map to wolrd positions
	var positionsInWorld  = []
	if aReturnCellCenterPosition:
		for positionInMap in aPositionsInMap:
			positionsInWorld.append(map_to_world_center(positionInMap))
	else:
		for positionInMap in aPositionsInMap:
			positionsInWorld.append(map_to_world(positionInMap))
	return positionsInWorld

func map_to_index(aPositionInMap:Vector2) -> float:
	# map position to map index
	return aPositionInMap.x + myMapRegion.size.x * aPositionInMap.y

func index_to_map(aIndex:float) -> Vector2:
	# map index to map position
	return Vector2(int(aIndex)%int(myMapRegion.size.x), floor(aIndex/myMapRegion.size.x))

func maps_to_indices(aPositionsInMap:Array) -> Array:
	# map positions to map indices
	var indices = []
	for positionInMap in aPositionsInMap:
		indices.append(map_to_index(positionInMap))
	return indices

func indices_to_maps(aIndices:Array) -> Array:
	# map indices to map positions
	var positionsInMap = []
	for index in aIndices:
		positionsInMap.append(index_to_map(index))
	return positionsInMap

func index_to_world(aIndex:float, aReturnCellCenterPosition:bool=false) -> Vector2:
	# map index to world position
	return map_to_world_configured(index_to_map(aIndex), aReturnCellCenterPosition)

func indices_to_worlds(aIndices:Array, aReturnCellCenterPosition:bool=false) -> Array:
	# map indices to world positions
	var positionsInWorld = []
	for index in aIndices:
		positionsInWorld.append(index_to_world(index, aReturnCellCenterPosition))
	return positionsInWorld

func is_walkable(aX:int, aY:int) -> bool:
	return get_cell(aX,aY) in myWalkableIds

func is_outside_map_bounds(aPositionInMap:Vector2) -> bool:
	return aPositionInMap.x < myMapRegion.position.x or aPositionInMap.y < myMapRegion.position.y \
		or aPositionInMap.x > myMapRegion.end.x or aPositionInMap.y > myMapRegion.end.y

func search_path_from_world(aStartPositionInWorld:Vector2, aTargetPosisionInWorld:Vector2, aReturnCellCenterPosition:bool=false) -> Array:
	# search path with world position and return path as world position
	var mapIndexPath = search_path_from_index(				\
		map_to_index( world_to_map(aStartPositionInWorld) ), 	\
		map_to_index( world_to_map(aTargetPosisionInWorld) )	\
	)
	var worldPositionPath = []
	if aReturnCellCenterPosition:
		for indexPosition in mapIndexPath:
			worldPositionPath.append(map_to_world_center(index_to_map(indexPosition)))
	else:
		for indexPosition in mapIndexPath:
			worldPositionPath.append(map_to_world(index_to_map(indexPosition)))
	# print_debug('world position path : ', worldPositionPath)
	return worldPositionPath

func search_path_from_map(aStartPositionInMap:Vector2, aTargetPosisionInMap:Vector2) -> Array:
	# search path with map position and return path as map position
	var mapIndexPath = search_path_from_index(	\
		map_to_index(aStartPositionInMap), 		\
		map_to_index(aTargetPosisionInMap)		\
	)
	var mapPositionPath = indices_to_maps(mapIndexPath)
	# print_debug('map position path : ', mapPositionPath)
	return mapPositionPath

func search_path_from_map_to_world(aStartPositionInMap:Vector2, aTargetPosisionInMap:Vector2, aReturnCellCenterPosition:bool=false) -> Array:
	# search path with map position and return path as world position
	var mapIndexPath = search_path_from_index(	\
		map_to_index(aStartPositionInMap), 		\
		map_to_index(aTargetPosisionInMap)		\
	)
	var worldPositionPath = indices_to_worlds(mapIndexPath, aReturnCellCenterPosition)
	# print_debug('world position path : ', worldPositionPath)
	return worldPositionPath

func search_path_from_index(aStartIndexInMap:int, aTargetIndexInMap:int) -> Array: # path index
	# search path with map index and return path as map index
	var mapIndexPath = myAstar.get_id_path(aTargetIndexInMap, aStartIndexInMap) # path is found with inversed order
	# print_debug('map index path : ', mapIndexPath)
	return mapIndexPath

# engine
func _ready():
	_priv_init_map()

func _input(aInputEvent:InputEvent):
	_priv_test_position(aInputEvent)

# dirty_work
func _priv_test_position(aInputEvent:InputEvent): # test position and tile unit
	if not Configuration.myDebug:
		return
	if aInputEvent.is_action_pressed("debug_test_map_position"):
		var world_pos = get_viewport().get_mouse_position()
		var map_pos = world_to_map(world_pos)
		var tile_index = get_cell(map_pos.x, map_pos.y)
		var map_index = map_to_index(map_pos)
		var map_pos_bac = index_to_map(map_index)
		var world_pos_bac = map_to_world(map_pos_bac)
		var world_pos_bac_center = map_to_world_center(map_pos_bac)
		print_debug('new test position :')
		print_debug('  world pos  : ' + String(world_pos))
		print_debug('  map pos    : ' + String(map_pos))
		print_debug('  tile index : ' + String(tile_index))
		print_debug('  map index  : ' + String(map_index))
		print_debug('  map pos    : ' + String(map_pos_bac) + ' (transoform backwards)')
		print_debug('  world pos  : ' + String(world_pos_bac) + ' (transoform backwards)')
		print_debug('  world pos  : ' + String(world_pos_bac_center) + ' (transoform backwards, cell center)')

func _priv_init_map():
	myMapRegion = get_used_rect()
	_priv_update_astar()

func _priv_update_walkables():
	printerr("Map2D._priv_update_walkables should be inherit and implemented")

func _priv_update_astar(): 
	_priv_update_walkables()
	var walkablePoints = []
	# find walkable positions && add them to myAstar
	for x in range(myMapRegion.position.x, myMapRegion.end.x):
		for y in range(myMapRegion.position.y, myMapRegion.end.y):
			if is_walkable(x, y):
				var walkablePosition = Vector2(x, y)
				var walkablePositionIndex = map_to_index(walkablePosition)
				walkablePoints.append( walkablePosition )
				myAstar.add_point(walkablePositionIndex, Vector3(x, y, 0.0))
				# print_debug('myAstar : adding walkable position ' + String(walkablePosition))
	# connect walkable positions
	for walkablePosition in walkablePoints:
		var walkablePositionIndex = map_to_index(walkablePosition)
		var relativePositions = PoolVector2Array([
			Vector2(walkablePosition.x + 1, walkablePosition.y),
			Vector2(walkablePosition.x - 1, walkablePosition.y),
			Vector2(walkablePosition.x, walkablePosition.y + 1),
			Vector2(walkablePosition.x, walkablePosition.y - 1)])
		for relativePosition in relativePositions:
			if is_outside_map_bounds(relativePosition):
				continue
			var relativePositionIndex = map_to_index(relativePosition)
			if not myAstar.has_point(relativePositionIndex): # skip  walkable points already in myAstar
				continue
			myAstar.connect_points(walkablePositionIndex, relativePositionIndex, false)
			# print_debug('myAstar : connect ' + String(walkablePositionIndex) + String(walkablePosition) + ' with ' + String(relativePositionIndex) +  String(relativePosition))
