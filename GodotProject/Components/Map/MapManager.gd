extends Node

# This script manage maps, need to run as @@ Singleton script @@

class MapInfo:
	var myName					:String				= ""
	var myInstance				:Map2D				= null

var myMaps				:Dictionary				= {}

# interfaces
func add_map(aName:String, aMap:Map2D):
	if _is_map_exist(aName):
		printerr("Map %s exists ! Trying to add duplicated" % aName)
		return
	var newMap = MapInfo.new()
	newMap.myName = aName
	newMap.myInstance = aMap
	_priv_add_map(newMap)

func get_map(aName:String) -> Map2D:
	if not _is_map_exist(aName):
		printerr("Map %s does not exist !" % aName)
		return null
	return _priv_get_map(aName).myInstance

# dirty works
func _is_map_exist(aName:String) -> bool:
	return myMaps.has(aName)

func _priv_add_map(aMap:MapInfo):
	myMaps[aMap.myName] = aMap

func _priv_get_map(aName:String) -> MapInfo:
	return myMaps[aName]

