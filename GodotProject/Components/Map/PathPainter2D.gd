extends Node2D

class_name PathPainter2D

var myPathColor			:Color	= Color(0,1,0)
var myPathWidth			:int	= 10
var myPathInWorld		:Array	= []
var myAntialiase		:bool	= true
var myVisible			:bool	= false

# interfaces
func show():
	myVisible = true
	update()
	set_process(true)

func hide():
	myVisible = false
	update()
	set_process(false)

func update_path(aPathInWorld):
	myPathInWorld = aPathInWorld
	update() # call update to call _draw()

# engine
func _draw():
	if not myVisible:
		return
	if myPathInWorld.size() > 2:
		var currentPoint = myPathInWorld[0]
		for i in range(myPathInWorld.size()-1):
			var nextPoint = myPathInWorld[i+1]
			draw_line(currentPoint, nextPoint, myPathColor, myPathWidth, myAntialiase)
			draw_circle(nextPoint, myPathWidth/2, myPathColor)
			currentPoint = nextPoint
