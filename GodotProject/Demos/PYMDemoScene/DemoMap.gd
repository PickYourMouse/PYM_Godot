extends Map2D

var myExitIds			:Array			= []
var myExits				:Dictionary		= {}

# interfaces
func get_exit_ids() -> Array:
	return myExitIds

func get_exits_by_id(aExitId) -> Array: # exits array (map position)
	return myExits[aExitId]

# engine
func _ready():
	# father engine function will be called first, then child engine function
	# i.e. _priv_update_walkables will be called first
	_priv_update_exits()
	MapManager.add_map("DemoMap", self)

func _input(aInputEvent:InputEvent):
	pass

# dirty works
func _priv_update_walkables():
	# child function will override father function, use ._priv_update_walkables to call fatcher function
	myWalkableIds.append(1) # background
	myWalkableIds.append(2) # exits

func _priv_update_exits():
	myExitIds.append(2)
	for exitId in myExitIds:
		myExits[exitId] = get_used_cells_by_id(exitId)

