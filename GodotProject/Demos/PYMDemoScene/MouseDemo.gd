extends KinematicBody2D

# required components
onready var myMoveController 		:MoveController2DBase		= $MoveController
onready var myAnimationPlayer 		:AnimationPlayer			= $AnimationPlayer

# interfaces
func detected():
	print("entered")
	pass

# engine
func _ready():
	myAnimationPlayer.set_current_animation('running')
	myMoveController.set_speed(120)

func _process(aDeltaTime:float):

	pass
