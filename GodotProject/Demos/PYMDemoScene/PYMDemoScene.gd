extends Node2D

onready var myPathPainter			:PathPainter2D		= $Map/PathPainter
onready var myMap					:Map2D				= $Map/Map
onready var myTestWASDMouse			:KinematicBody2D	= $Mice/MouseWASDDemo
onready var myTestPathFollowMouse	:KinematicBody2D	= $Mice/MousePathFollowDemo

onready var myExitArea									= load("res://Demos/PYMDemoScene/ExitArea.tscn")

var myStarted					:bool	= false
var myExitAreaInitialized		:bool	= false
var myTestWASDMoveStarted		:bool	= false
var myTestPathFollowStarted		:bool	= false

# engine
func _ready():
	print("WASD move mouse : ", myTestWASDMouse)
	print("Path follow move mouse : ", myTestPathFollowMouse)
	pass

func _process(delta):
	priv_init_exit_area()
	test_wasd_move()
	test_path_follow_move()
	pass

func _on_Area2D_area_entered(body):
	print("_on_Area2D_area_entered :", body)

func priv_init_exit_area():
	if not myExitAreaInitialized:
		var exits = myMap.get_exits_by_id(2)
		for e in exits:
			var pos = myMap.map_to_world_center(e)
			var area = myExitArea.instance()
			area.set_size(Vector2(30, 30))
			area.set_position(pos)
			area.connect("body_entered", self, "_on_Area2D_area_entered")
			myMap.add_child(area)
		myExitAreaInitialized = true

func test_wasd_move():
	if not myTestWASDMoveStarted:
		myTestWASDMoveStarted = true
	pass

func test_path_follow_move():
	if not myTestPathFollowStarted:
		var exits = myMap.get_exits_by_id(2)
		var bornExit = exits[0]
		var hideExit = exits[1]
		var runningPath = myMap.search_path_from_map_to_world(bornExit, hideExit, true)
		
		myTestPathFollowMouse.get_node("MoveController").update_path(runningPath)
		
		myPathPainter.update_path(runningPath)
		myPathPainter.show()
		
		myTestPathFollowStarted = true
	pass
